// SPDX-FileCopyrightText: 2022 Ethaks <ethaks@pm.me>
//
// SPDX-License-Identifier: EUPL-1.2

module.exports = {
  semi: true,
  //trailingComma: "all",
  singleQuote: false,
  printWidth: 100,
  tabWidth: 2,
};
