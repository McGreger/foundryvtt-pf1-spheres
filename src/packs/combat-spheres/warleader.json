{
  "_id": "vAdZC2sURcno3gdv",
  "name": "Warleader",
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "warleader"
    }
  },
  "pages": [
    {
      "name": "Warleader",
      "type": "text",
      "title": {
        "show": false,
        "level": 1
      },
      "text": {
        "format": 1,
        "content": "<img src=\"modules/pf1spheres/assets/icons/spheres/warleader.webp\" style=\"float:right;border:none;outline:none\" />Practitioners of the Warleader sphere learn techniques for organizing, rallying, and directing their allies in battle. Regardless of whether they are charismatic warriors leading from the front or canny tacticians directing their troops from a secure position overlooking the battlefield, it is the superior talent for directing their troops to coordinate the correct response that makes them invaluable forces on the battlefield. When you gain the Warleader sphere, you gain 5 ranks in the Diplomacy skill, plus 5 ranks per additional talent spent in the Warleader sphere (maximum ranks equal to your total Hit Dice). If you already have ranks in the Diplomacy skill you may immediately retrain them, but you do not get to retrain when only temporarily gaining talents, such as through the armiger’s customized weapons class feature.\n<h2 id=\"toc0\"><span><span style=\"color:#993300\">Tactics</span></span></h2>\n<p>Tactics are coordinated battle plans that require continuing direction from the practitioner to maintain. A creature must have line of sight to, and be able to see, the practitioner to benefit from a tactic. Beginning a tactic is a move action, and it can be maintained each round as a move or swift action. Once activated, you may switch between any tactics you know each time you use a swift action to maintain an ongoing tactic. Tactics affect all allied creatures within a radius of 10 ft. + 5ft. per rank in Diplomacy you possess, and may be centered on any square you have both line of sight and line of effect to. You may recenter an ongoing tactic at a new location as part of the swift action used to maintain it. The benefits of your tactics end immediately if you are helpless, killed, paralyzed, rendered unconscious, or stunned.</p>\n<p>When you first gain the Warleader sphere, you gain the following tactic:</p>\n<h4 id=\"toc1\"><span>Aggressive Flanking</span></h4>\n<p>While within the affected area of this tactic, allied creatures are considered to be flanking as long as they both threaten the same creature, regardless of their comparative positioning.</p>\n<h2 id=\"toc2\"><span><span style=\"color:#993300\">Shouts</span></span></h2>\n<p>Shouts are sound-based effects centered on the practitioner that affect creatures in an area of effect centered on the practitioner with a radius of 10 ft. + 5 ft. per 2 ranks in Diplomacy the practitioner possesses. The practitioner may choose whether or not to include himself in the effects of his shout. The effects of shouts last for a number of rounds equal to 1 + 1 for every 4 ranks in Diplomacy you possesses, and use your ranks in Diplomacy instead of your base attack bonus when determining any saving throws. Unless otherwise noted, using a shout is a standard action. Deaf characters or characters otherwise lacking the ability to hear gain a +5 bonus on all saves against shout effects, and must attempt a saving throw against a shout even if its effects would be beneficial. Practitioners in the area of a silence spell or otherwise unable to make a sound cannot use shouts until they are once more able to be heard.</p>\n<p>While some shouts only affect allies or enemies, others affect all targets within their area of effect. When performing such a shout, you may spend a move action to warn your allies to cover their ears and negate the effects, but doing so gives enemies within the area of effect a +5 bonus to their saving throw against the shout’s effects. Some shouts may require you to expend your martial focus, as described in their entry.</p>\n<p>When you first gain the Warleader sphere, you gain the following shout:</p>\n<h4 id=\"toc3\"><span>Fierce Shout</span></h4>\n<p>When you use this shout, you and all affected allies gain a +1 morale bonus on damage rolls on their first attack each turn. For every 2 ranks in Diplomacy you possess, this morale bonus to damage is increased by +1. This is a mind-affecting emotion effect.</p>\n<hr /><br />\n<br /><b>Warleader Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.TKTCnmNseacFJBh7]{Advanced Notice}</li>\n<li>@Compendium[pf1spheres.combat-talents.SMHRVcclLN09PwuK]{Bandmaster}</li>\n<li>@Compendium[pf1spheres.combat-talents.kehjGGYz6LHUU9Am]{Breath Support}</li>\n<li>@Compendium[pf1spheres.combat-talents.K2OxKBR5kobi0tmB]{Focusing Tactics}</li>\n<li>@Compendium[pf1spheres.combat-talents.23LtDpCkXoHmiqm0]{Heraldry}</li>\n<li>@Compendium[pf1spheres.combat-talents.tdCyNXPSdtyyVHjY]{Inside Voice}</li>\n<li>@Compendium[pf1spheres.combat-talents.oLgvBjjHu8GakgTf]{Lasting Oration}</li>\n<li>@Compendium[pf1spheres.combat-talents.5qHOkUjC5W80T2FT]{Marauding Monkey}</li>\n<li>@Compendium[pf1spheres.combat-talents.fcO5uiU256yN2mAZ]{Persisting Influence}</li>\n<li>@Compendium[pf1spheres.combat-talents.nLfYFCFcwWyVxg9A]{Projecting Voice}</li>\n<li>@Compendium[pf1spheres.combat-talents.cAJ1awv9BwT5zzG9]{Roaring Reservoir}</li>\n<li>@Compendium[pf1spheres.combat-talents.nR8JMO92r5HmHsDQ]{Semaphore}</li>\n<li>@Compendium[pf1spheres.combat-talents.O5q6kHCJNmJHWA81]{Strategically Distant Examination}</li>\n<li>@Compendium[pf1spheres.combat-talents.ZWGEkaCcsy9TjNHy]{Triumph}</li>\n<li>@Compendium[pf1spheres.combat-talents.todEEFqrr36Vzsdo]{Verbal Commands}</li>\n<li>@Compendium[pf1spheres.combat-talents.iCFpOR2CemNAkgLR]{Verbal Counter}</li>\n</ul><br /><b>Shout Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.FzrAs3zN3EzSdFbe]{Call Attention}</li>\n<li>@Compendium[pf1spheres.combat-talents.pHMN2HtZLbHmDhQv]{Cry Of Confrontation}</li>\n<li>@Compendium[pf1spheres.combat-talents.hNBD3SPs5tU9ppvL]{Disarming Roar}</li>\n<li>@Compendium[pf1spheres.combat-talents.8XC7FBNJhWYuoQtt]{Dispiriting Roar}</li>\n<li>@Compendium[pf1spheres.combat-talents.tOPPKI0kkD3ph3Zf]{Distracting Cacophony}</li>\n<li>@Compendium[pf1spheres.combat-talents.EwXrKAssJmSfig0D]{Focusing Cry}</li>\n<li>@Compendium[pf1spheres.combat-talents.Bdx7CJZMeuLgfksr]{Frightful Roar}</li>\n<li>@Compendium[pf1spheres.combat-talents.OCHJva8T9yrSbMPG]{Harangue}</li>\n<li>@Compendium[pf1spheres.combat-talents.O0rp4ibVnTRniS3S]{Inspiring Speech}</li>\n<li>@Compendium[pf1spheres.combat-talents.rKIgk1O84wTQA1KG]{Invigorating Call}</li>\n<li>@Compendium[pf1spheres.combat-talents.6e7k5hOvzlDEoygh]{Protective Warning}</li>\n<li>@Compendium[pf1spheres.combat-talents.jzxZO29bYTZyrZ6r]{Rallying Speech}</li>\n<li>@Compendium[pf1spheres.combat-talents.jn815SBbP6RoVwVj]{Rousing Claxon}</li>\n</ul><br /><b>Tactic Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.HZFwUIylpy32Z8Mb]{Courier’s Dash}</li>\n<li>@Compendium[pf1spheres.combat-talents.9h7OnckZZlwzIAK7]{Coordinated Reflexes}</li>\n<li>@Compendium[pf1spheres.combat-talents.B3FdYzZG9425up1R]{Covert Operations}</li>\n<li>@Compendium[pf1spheres.combat-talents.TZPPyHzRYQGrQxKP]{Deadly Herdsman}</li>\n<li>@Compendium[pf1spheres.combat-talents.l3YcG3RoPJF2vhzt]{Fortifying Phalanx}</li>\n<li>@Compendium[pf1spheres.combat-talents.rCru5LjZVbivycCR]{Masterful Coordination}</li>\n<li>@Compendium[pf1spheres.combat-talents.B1KgvdFpsb4POwew]{Militant Will}</li>\n<li>@Compendium[pf1spheres.combat-talents.9GYb2x0vhAGAHaKP]{Preparation}</li>\n<li>@Compendium[pf1spheres.combat-talents.iYQWFEg6cyleWZ4W]{Shieldbrothers}</li>\n<li>@Compendium[pf1spheres.combat-talents.f3NynytErEdQcL7f]{Synchronized Strikes}</li>\n<li>@Compendium[pf1spheres.combat-talents.NyvvEd1QIN0D8mNV]{Unified Morale}</li>\n</ul><br /><b>Legendary Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.5NPgPt09fXHVlwny]{Armies Of The Dead}</li>\n<li>@Compendium[pf1spheres.combat-talents.eRvEmC66lSD1NYZL]{Darkland’s Cry}</li>\n<li>@Compendium[pf1spheres.combat-talents.qkUjj53FVRkEIPU4]{Explosive Ululation}</li>\n<li>@Compendium[pf1spheres.combat-talents.mUbJpyqD7Ixhm4Mo]{Legion Unending}</li>\n<li>@Compendium[pf1spheres.combat-talents.VqDqUiFA1PibIQ3g]{Master’s Aura}</li>\n<li>@Compendium[pf1spheres.combat-talents.YQ54PPnD6llEOUes]{Phantom Operatives}</li>\n<li>@Compendium[pf1spheres.combat-talents.MBEvUafamlAX57ms]{Piercing Voice}</li>\n<li>@Compendium[pf1spheres.combat-talents.ZmxHb7avgisDLPw2]{Recall Spirit}</li>\n<li>@Compendium[pf1spheres.combat-talents.RuOI4oLKpNZCVnTU]{Resonating Chorus}</li></ul>"
      },
      "_id": "1Y6shgQvz37Fa0nR",
      "image": {},
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "sort": 0,
      "ownership": {
        "default": -1
      },
      "flags": {}
    }
  ],
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "0.82.2",
    "coreVersion": "10.285",
    "createdTime": 1663169292942,
    "modifiedTime": 1663181691623,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
